FROM node:8.9.4-alpine

WORKDIR /src

COPY . .

RUN npm i -g nodemon \
    && npm i -g babel-cli

RUN npm install

EXPOSE 8000

#CMD ["babel-node", "app.js"]
