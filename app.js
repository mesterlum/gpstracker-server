import express from 'express'
import bodyParser from 'body-parser'
import http from 'http'
import mongoose from 'mongoose'
import api from './routes'
import cors from 'cors'
import socket from 'socket.io'

import confSocket from './socket'

//Models
import './models'
//config express
const app = express()
const server = http.createServer(app)

//Environment
app.set('environment', process.env.NODE_ENV)

//Config
import config from './config'

//body-parser
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// cors
app.use(cors())

//config
const { PORT, URL_DB } = config

//Logger
if (app.get('environment') !== 'production')
    app.use(require('morgan')())

//Api
app.use('/api', api)

// sock
const sock = socket(server)
confSocket(sock)


mongoose.connect(URL_DB, { useNewUrlParser: true, useCreateIndex: true }, err => err ? console.log(err): console.log('Conexion con exito'))
server.listen(PORT, '0.0.0.0')
