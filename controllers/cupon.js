import cuponModel, { cuponGivedModel } from '../models/cupons'
import userModel from '../models/users'


/*
    it should be automatically called when someone paid,
    it is not necessary be logged to call this endpoint

    * At the moment the register is directly

*/

function registerCupon(req, res) {
    const { code, amount, totalCupons } = req.body
    if (!code || !amount || !totalCupons)
        return res.status(400).send({ message: 'Tiene que enviar code, amount, totalCupons' })

    if (code.length !== 8)
        return res.status(400).send({ message: '8 caracteres para el cupon' })

    const newCupon = new cuponModel({
        code: code.toUpperCase(),
        amount,
        totalCupons
    })
    newCupon.save((err, cupon) => {
        if (err) return res.status(500).send({ message: 'Internal error for register cupon', err })
        return res.status(200).send({ cupon })
    })

}
async function redeemCoupon(req, res) {
    const { code } = req.body
    if (!code)
        return res.status(400).send({ message: 'Tiene que enviar code' })

    const cupon = await cuponModel.findOne({ code })
    if (!cupon)
        return res.status(404).send({ message: 'cupon not found' })
    const user = await userModel.findById(req.user._id).select('cuponMoney')
    const userInCupon = await cuponGivedModel.findOne({ user: req.user, cupon: cupon })

    if (userInCupon)
        return res.status(302).send({ message: 'usted ya registro este cupon' })

    if (!user)
        return res.status(404).send({ message: 'user not found' })

    cupon.gived++
    if (cupon.gived > cupon.totalCupons)
        return res.status(202).send({ message: 'Limite of cuppons gived superate' })

    new cuponGivedModel({
        user,
        cupon
    }).save(err => {
        if (err)
            if (err) return res.status(500).send({ message: 'Internal error for register cupon', err })
        cupon.save(err => {
            if (err) return res.status(500).send({ message: 'Internal error for register cupon', err })
            user.cuponMoney += cupon.amount
            user.save((err, user) => {
                if (err) return res.status(500).send({ message: 'Internal error for register cupon', err })
                return res.status(200).send({ user, amount: cupon.amount })
            })
        })
    })


}



export default {
    registerCupon,
    redeemCoupon
}