import bcrypt from 'bcrypt'
import employeeModel from '../models/employee'
import { encodeToken } from '../services'
import uid from 'uid'

const HASH_SALT = 10

function logIn(req, res) {
    
    const { idEmployee, password } = req.body
    if (!idEmployee || !password) return res.status(400).send({message : 'Escriba el id de empleado y contraseña'})
    
    employeeModel.findOne({idEmployee})
        .exec((err, employee) => {
            
            if (err) return res.status(500).send({message : '[Internal Error for Login]'})
            if (!employee) return res.status(404).send({message : 'id  y/o contraseña incorrecta'})
            if (employee.status === 'B')
                return res.status(400).send({message: 'Esta cuenta esta dada de baja'})
            bcrypt.compare(password, employee.password).then((compare) => {
                
                if (!compare)
                    return res.status(404).send({message : 'Usuario y/o contraseña incorrecta'})

                employee.password = undefined
                employee.__v = undefined
                return res.status(200).send({message : 'Login correcto', employee : encodeToken(employee)})
            })
            .catch(err => res.status(500).send({message : '[Internal Error for Login]'}))
            
        })
}


/*
    *This method recived the register from news employees
    @param firstName
    @param lastName
    @param typeEmployee

*/

function signUp(req, res) {
    
    if (!req.body.firstName || !req.body.lastName || !req.body.typeEmployee) return res.status(400).send({message : 'Llene todos los campos'})
    if (req.user.typeEmployee !== 'A')
        return res.status(400).send({message: 'Usted no tiene permsisos para registrar a un empleado'})
    if (req.body.typeEmployee !== 'C')
        return res.status(400).send({message: 'Por favor introduce un tipo de empleado valido'})
    const { firstName, lastName, typeEmployee} = req.body
    const password = `${Math.floor((Math.random() * 9) + 1)}${Math.floor((Math.random() * 9) + 1)}${Math.floor((Math.random() * 9) + 1)}${Math.floor((Math.random() * 9) + 1)}`
    bcrypt.hash(password, HASH_SALT).then((hash) => {
        let newemployee = new employeeModel({
            password : hash,
            idEmployee:  uid(8),
            firstName,
            lastName,
            employeeRegister: req.user._id,
            typeEmployee
        })
        newemployee.save((err, employee) =>{
            if (err) return res.status(500).send({message: 'Internal Error for register', err})
            return res.status(200).send({message : 'Registro con exito', employee, passwordPlain: password})
        })

    })

    
}


export default {
    signUp,
    logIn
}