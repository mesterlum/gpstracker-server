import { routModel, pointModel } from '../models/routs'
import vehicleModel from '../models/vehicles'

/*
    *This method recived from URL params for generate and save in database
    @param nameRout
    @param pointsGeolocation
    @param color
    @param colorCode
*/

function registerRout(req, res) {

    if (!req.body.pointsGeolocation || !req.body.color || !req.body.colorCode || !req.body.frequency)
        return res.status(400).send({ message: 'Tiene que enviar todos los parametros' })

    if (req.user.typeEmployee !== 'A')
        return res.status(401).send({ message: 'Usted no tiene permisos para registrar una ruta' })

    const { pointsGeolocation, pointsToCross } = req.body
    const newRout = new routModel({
        pointsGeolocation,
        color: req.body.color,
        colorCode: req.body.colorCode,
        employeeRegister: req.user._id,
        pointsToCross,
        frequency: req.body.frequency
    })

    newRout.save((err, rout) => {
        if (err) console.log(err)
        return res.status(200).send({ message: 'Se ha agregado la ruta correctamente', rout })
    })
}


// Validations will be required in the future, for the moment the documents is taken by the request
function registerPoint(req, res) {

    //console.log(req.body)
    if (req.user.typeEmployee !== 'A')
        return res.status(401).send({ message: 'Usted no tiene permisos para agregar una referencia' })

    if (!req.body.data) {
        return res.status(400).send({ message: 'Tiene que enviar todos los parametros' })
    }


    pointModel.insertMany(req.body.data, (err, docs) => {
        if (err) return res.status(500).send({ err })
        return res.status(200).send(docs)
    })

}

// In the moment the permise isn't necessarie
function getPoints(req, res) {
    pointModel.find({}, (err, data) => {
        if (err) return res.status(500).send({ err })
        return res.status(200).send(data)
    })
}

/*
    *This method recieve the routes and filters them by colors
    -Temporally returns only routes based in their color
    @param routs

*/

function getRouts(req, res) {


    const { routs } = req.query
    var find = {}
    if (routs) {
        let routsSplit = routs.split(',')
        let objSearch = []
        routsSplit.map(data => objSearch.push({ color: data }))
        find = { $or: objSearch }
    }
    routModel.find(find).populate({ path: 'pointsToCross', select: 'name' }).exec((err, data) => {
        if (err) return res.status(500).send({ message: 'Internal error', err })
        return res.status(200).send({ data })
    })

}

function getRoutByPoint(req, res) {

    routModel.find({
        pointsToCross: {"$in": req.params.point}
    }).populate({ path: 'pointsToCross' }).exec((err, data) => {
        if (err) return res.status(500).send({ message: 'Internal error', err })
        return res.status(200).send({ data })
    })
}


export default {
    registerRout,
    getRouts,
    registerPoint,
    getPoints,
    getRoutByPoint
}