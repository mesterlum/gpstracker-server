import modelTracking from '../models/tracking'
import vehiclesModel from '../models/vehicles'


function registerTracking(req,res){
    vehiclesModel.findOne({imei: req.body.imei},{_id: 1}, (err,vehicle) => {
        if (err) throw err;
         const newTracking = new modelTracking({
            vehicleId: vehicle._id,
            geolocation: req.body.geolocation,
            speed: req.body.speed
         })
         newTracking.save((err) => {
             if (err) throw err;
             return res.status(200).send({ok: true})
         } )
    })

}


export default {
    registerTracking
}