import travelModel from '../models/travel'
import userModel from '../models/users'
import { vehicleModel } from '../models/vehicles'
import { Types } from 'mongoose'



async function registerTravel(req, res) {

    const { typePayment, vehicle } = req.body

    if (!typePayment || !vehicle)
        return res.status(500).send({ message: 'all fields are necessary { typePayment, vehicle }' })

    if (typePayment != 1) // it isn't electronic money
        return res.status(500).send({ message: 'for the moment we are not accepting another kind of payment different of electronic cash' })

    if (!Types.ObjectId.isValid(vehicle))
        return res.status(500).send({ message: 'id not valid vehicle' })

    const userPayment = await userModel.findById(req.user._id)
        .populate({
            path: 'typeUser',
            select: 'deduction'
        })
        .select('money cuponMoney')


    if (!userPayment)
        return res.status(404).send({ message: 'user not found' })

    const vehiclePayment = await vehicleModel.findById(vehicle)
        .populate({
            path: 'service',
            select: 'price'
        })
        .select('rout service')

    if (!vehiclePayment)
        return res.status(404).send({ message: 'vehicle not found' })


    var amountPaid = 0
    let discount = vehiclePayment.service.price * (userPayment.typeUser.deduction / 100)
    
    if (userPayment.cuponMoney >= vehiclePayment.service.price) {
        amountPaid = vehiclePayment.service.price - discount
        userPayment.cuponMoney -= amountPaid
    }
    else if (userPayment.money >= vehiclePayment.service.price) {
        amountPaid = vehiclePayment.service.price - discount
        userPayment.money -= amountPaid
    }
    else
        return res.status(402).send({ message: 'Usted no tiene suficiente fondos en su cuenta' })

    new travelModel({
        user: userPayment,
        vehicle: vehiclePayment,
        rout: vehiclePayment.rout
    }).save(async (err) => {
        if (err)
            return res.status(500).send({ message: 'internal error', err })
        const saveUser = await userPayment.save()
        if (!saveUser)
            return res.status(500).send({ message: 'internal error', err })
        return res.status(200).send({
            message: 'ok',
            amountPaid
        })
    })
}


export default {
    registerTravel
}