import bcrypt from 'bcrypt'
import { isEmail } from 'validator'
import UserModel, { typeUserModel } from '../models/users'
import fs from 'fs'
import uid from 'uid'
import path from 'path'
import { encodeToken, emailConfiguration } from '../services'

const HASH_SALT = 10

function logIn(req, res) {

    const { email, password } = req.body
    if (!email || !password) return res.status(400).send({ message: 'Escriba el usuario y contraseña' })

    UserModel.findOne({ email })
        .exec((err, user) => {
            if (err) return res.status(500).send({ message: '[Internal Error for Login]' })
            if (!user) return res.status(404).send({ message: 'Usuario y/o contraseña incorrecta' })
            if (user.status === 'P' || user.status === 'B')
                return res.status(400).send({ message: 'Esta cuenta no esta activada o esta bloqueada' })

            bcrypt.compare(password, user.password).then((compare) => {

                if (!compare)
                    return res.status(404).send({ message: 'Usuario y/o contraseña incorrecta' })
                user.password = undefined
                user.codeValidation = undefined
                user.__v = undefined
                return res.status(200).send({ message: 'Login correcto', user: encodeToken(user) })
            })
                .catch(err => res.status(500).send({ message: '[Internal Error for Login]' }))

        })
}

/*
    *This method recived the register from news users
    @param email
    @param password
    @param firstName
    @param lastName

*/

function signUp(req, res) {


    if (!req.body.email || !req.body.password || !req.body.password.lenght > 2 || !req.body.firstName ||
        !req.body.lastName) return res.status(400).send({ message: 'Llene todos los campos' })

    const { email, password, firstName, lastName } = req.body

    if (!isEmail(email))
        return res.status(400).send({ message: 'Escriba el correo correctamente' })

    UserModel.findOne({ email }, (err, userExists) => {

        if (err) return res.status(500).send({ message: '[Internal Error for Login]' })
        if (userExists)
            return res.status(202).send({ message: 'Este email ya existe' })

        bcrypt.hash(password, HASH_SALT).then((hash) => {

            let newUser = new UserModel({
                email,
                password: hash,
                firstName,
                lastName,
                typeUser: '5ce1cfd82000a456bd3d0467' // Temporary
            })
            newUser.save((err, user) => {
                if (err) return res.status(500).send({ message: '[Internal Error for Login]' })
                //Send Email for confirmation
                emailConfiguration(user.email, user.firstName, user.codeValidation, user._id)
                    .catch(val => res.status(500).send({ message: 'Internal error google' }))
                return res.status(200).send({ message: 'Registro con exito, se ha enviado un correo activar la cuenta', user })
            })

        })

    })
}

/*
    *This method change status of the Account
    @param idUser
    @param code  -- Code of the activation
*/

function validateAccount(req, res) {
    if (!req.query.idUser || !req.query.code)
        return res.status(400).send({ message: 'Faltan parametros' })

    const { idUser, code } = req.query

    UserModel.findById(idUser)
        .exec((err, user) => {
            if (err) return res.status(500).send({ message: '[Internal Error for Activation]' })
            if (user.status === 'A' || user.status === 'B')
                return res.status(403).send({ message: 'No puedes activar una cuenta activa o baneada' })
            if (req.query.code != user.codeValidation)
                return res.status(400).send({ message: 'Codigo de verificacion erronea' })
            UserModel.findByIdAndUpdate(idUser, { status: 'A' }, err => err ? res.status(500).send({ message: 'Internal Error' }) : res.status(200).send({ message: 'Usuario Activado' }))

        })
}

/*
    *This method recived image for avatar for profiles users
    *If image > 3.0MB return image and avatar
    @param fileImage
    
*/

function uploadImageForAvatar(req, res) {
    const { avatar } = req.files
    const extension = avatar.name.split('.').pop()

    if (extension !== 'png' && extension !== 'jpeg' && extension !== 'jpg')
        return res.status(400).send({ message: 'Debe enviar una imagen de formato valido o una imagen' })

    if (avatar.size / 1024 > 3072)
        return res.status(400).send({ message: 'La imagen es muy pesada' })
    const nameImage = uid(30) + "." + extension
    fs.rename(avatar.path, __dirname + '/../assets/profilesAvatars/' + nameImage)

    UserModel.findByIdAndUpdate(req.user._id, { avatar: nameImage })
        .exec((err, user) => err ? res.status(500).send({ message: 'Internal error' }) :
            UserModel.findByIdAndUpdate(req.user._id, { $push: { historyAvatars: user.avatar } }, err =>
                err ? res.status(500).send({ message: 'Internal error' }) : res.status(200).send({ message: 'Update Avatar Succes' }))

        )

}

function getAvatar(req, res) {
    UserModel.findById(req.user._id, { avatar: 1 })
        .exec((err, user) => {
            if (err) return res.status(500).send({ message: 'Internal error' })
            const pathAvatar = path.resolve() + `/assets/profilesAvatars/${user.avatar}`
            if (fs.existsSync(pathAvatar))
                return res.sendFile(pathAvatar)
            else
                return res.sendFile(path.resolve() + `/assets/profilesAvatars/default.jpg`)
        })
}

function registerTypeUser(req, res) {

    if (req.user.typeEmployee !== 'A')
        return res.status(401).send({ message: 'Usted no tiene permisos para registrar un tipo de usuario' })

    const { description, deduction } = req.body
    if (!description || !deduction)
        return res.status(400).send({ message: 'Faltan parametros { description, deduction }' })

    new typeUserModel({
        description,
        deduction
    })
        .save((err, type) => {
            if (err)
                return res.status(500).send({ message: 'Internal error', err })
            return res.status(200).send({ type })
        })
}

function getInformationUser(req, res) {
    UserModel.findById(req.user)
        .populate({
            path: 'typeUser',
            select: 'description'
        })
        .select('typeUser money cuponMoney phone firstName lastName email dateRegister')
        .exec((err, user) => {
            if (err)
                return res.status(500).send({ message: 'internal error', err })
            if (!user)
                return res.status(404).send({ message: 'user not found' })
            return res.status(200).send(user)
        })
}


export default {
    logIn,
    signUp,
    validateAccount,
    uploadImageForAvatar,
    getAvatar,
    registerTypeUser,
    getInformationUser
}