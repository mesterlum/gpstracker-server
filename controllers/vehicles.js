import { vehicleModel, servicesVehicleModel } from '../models/vehicles'
import { Types } from 'mongoose'

function registerVehicle(req, res) {

    const { imei, rout, employeeOperator, service, matricule } = req.body

    if (!imei || !rout || !employeeOperator || !service || !matricule)
        return res.status(400).send({ message: 'Tiene que enviar imei, rout y employeeOperator' })

    if (req.user.typeEmployee !== 'A')
        return res.status(400).send({ message: 'Usted no puede registrar un vehiculo' })

    const newVehicle = new vehicleModel({
        imei,
        rout,
        employeeRegister: req.user._id,
        employeeOperator,
        service,
        matricule
    })

    newVehicle.save((err, vehicle) => {
        if (err) return res.status(500).send({ message: 'Internal error for register vehicle', err })
        return res.status(200).send({ message: 'Vehiculo registrado con éxito', vehicle })
    })

}

function registerService(req, res) {
    const { price, description } = req.body
    if (!price)
        return res.status(400).send({ message: 'envia price' })

    const newService = new servicesVehicleModel({
        price,
        description
    })
    newService.save((err, service) => {
        if (err) if (err) return res.status(500).send({ message: 'Internal error for register service', err })
        return res.status(200).send({ message: 'Service registrado con exito', service })
    })

}

function getVehicle(req, res) {
    const { id } = req.params

    if (!id)
        return res.status(400).send({ message: 'Necesitas enviar id' })

    const data = Types.ObjectId.isValid(id) ? { _id: id } : { matricule: id }

    vehicleModel.findOne(data, {})
        .populate({
            path: 'employeeOperator',
            select: 'firstName lastName -_id dateRegister'
        })
        .populate({
            path: 'service',
            select: 'price -_id'
        })
        .populate({
            path: 'rout',
            select: 'color colorCode pointsToCross -_id',
            populate: {
                path: 'pointsToCross',
                select: 'name -_id'
            }
        })
        .select('employeeOperator service rout matricule')
        .exec((err, vehicle) => {
            if (err) return res.status(500).send({ message: 'Internal error for get vehicle', err })
            return res.status(200).send({ vehicle })

        })




}

export default {
    registerVehicle,
    registerService,
    getVehicle
}