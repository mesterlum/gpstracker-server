import { decodeToken } from './services'

export default (req,res,next)=> {

    if (!req.headers.authorization)
        return res.status(403).send({message: 'Usted no tiene autorizacion'})

    const token = req.headers.authorization.split(' ')[1]
    decodeToken(token)
        .then(user =>{
            req.user = user.sub
            next()
        })
        .catch(response =>{
            res.status(response.status).send({message: response.message})

        })
}