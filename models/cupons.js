import mongoose, { Schema } from 'mongoose'
import { todayStamp } from '../utils'

const Cupon = new Schema({
    code: {
        type: String,
        required: true,
        minlength: 8,
        maxlength: 8,
        unique: true
    },
    amount: {
        type: Number,
        required: true
    },
    totalCupons: {
        type: Number,
        required: true
    },
    gived: {
        type: Number,
        default: 0
    },
    status: {
        type: String,
        enum: ['A', 'B'], // A = Available, C = Canceled
        default: 'A',
    },
    createdAt: {
        type: Number,
        default: todayStamp
    }
})

const CuponGived = new Schema({
    cupon: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'Cupon'
    },
    user: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    }
})


export const cuponGivedModel = mongoose.model('CuponGived', CuponGived)
export default mongoose.model('Cupon', Cupon)