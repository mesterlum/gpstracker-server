import mongoose,{ Schema } from 'mongoose'
import { todayStamp } from '../utils'

/*
    This model is for Employees  
    *idEmployee generate random id

*/

const Employee = new Schema({
    idEmployee: {
        type : String,
        unique : true
    },
    password: {
        type : String,
        required: true
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    typeEmployee: {
        type: String,
        enum: ['C', 'A'], // C = Chofer, A = Admin
        required: true
    },
    status: {
        type: String,
        enum: ['A','B'],
        default: 'A',
        required: true
    },
    employeeRegister: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'Employee'
    },
    dateRegister: {
        type: Number,
        default: todayStamp
    }

})

export default mongoose.model('Employee', Employee)