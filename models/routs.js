import mongoose,{ Schema } from 'mongoose'

import { todayStamp } from '../utils'

const Rout = new Schema({
    pointsGeolocation: {
        type: Array,
        required: true
    },
    employeeRegister: {
        type: Schema.Types.ObjectId,
        ref: 'Employee',
        required: true
    },
    dateRegister: {
        type: Number,
        default: todayStamp
    },
    color: {
        type: String,
        required: true,
        unique: true
    },
    pointsToCross: [{
        type: Schema.Types.ObjectId,
        ref: 'Point',
    }],
    frequency: {
        type: Number,
        required: true
    },
    colorCode: {
        type: String,
        required: true,
        unique: true
    }
})

const Point = new Schema({
    name: {
        type: String,
        required: true
    },
    geolocation: {
        type: Object,
        required: true
    },
    registerAt: {
        type: Number,
        default: todayStamp
    }
})

export const pointModel = mongoose.model('Point', Point)
export const routModel = mongoose.model('Rout', Rout)