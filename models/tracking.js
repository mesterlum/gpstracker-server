import mongoose,{ Schema } from 'mongoose'
import { todayStamp } from '../utils'

const Tracking = new Schema({

    vehicleId : {
        type: Schema.Types.ObjectId,
        ref: 'Vehicle'
    },
    registerDate : {
        type : Number,
        default : todayStamp
    },
    geolocation : {
       latitud : {
            type: Number,
            required : true
       },
       longitud : {
            type: Number,
            required : true
       }
    },
    speed: {
        type: Number,
        required: true
    }

})

export default mongoose.model('Tracking', Tracking)