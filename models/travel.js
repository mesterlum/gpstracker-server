import mongoose, { Schema } from 'mongoose'
import { todayStamp } from '../utils'

const Travel = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    vehicle: {
        type: Schema.Types.ObjectId,
        ref: 'Vehicle',
        required: true
    },
    rout: {
        type: Schema.Types.ObjectId,
        ref: 'Rout',
        required: true
    },
    registerAt: {
        type: Number,
        default: todayStamp
    }
})


export default mongoose.model('Travel', Travel)