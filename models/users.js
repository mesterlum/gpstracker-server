import mongoose, { Schema } from 'mongoose'

import { todayStamp } from '../utils'

const UsersSchema = new Schema({
    email : {
        type: String,
        max : 60,
        unique : true,
        lowercase : true,
        required: true
    },
    phone : {
        type: String,
        min: 10
    },
    password: {
        type: String,
        min: 3,
        required: true
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    status: {
        type: String,
        enum: ['A', 'B', 'P'],
        default: 'P'
    },
    avatar: {
        type: String,
        default: 'none',
    },
    codeValidation: {
        type: String,
        default: `${Math.floor((Math.random() * 9) + 1)}${Math.floor((Math.random() * 9) + 1)}${Math.floor((Math.random() * 9) + 1)}${Math.floor((Math.random() * 9) + 1)}`
    },
    historyAvatars:[{
        type: String
    }],
    money: {
        type: Number,
        default: 0
    },
    cuponMoney: {
        type: Number,
        default: 0
    },
    typeUser: {
        type: Schema.Types.ObjectId,
        ref: 'TypeUser',
        required: true
    },
    dateRegister: {
        type: Number,
        default: todayStamp
    }
})

const TypeUser = new Schema({
    description: {
        type: String,
        required: true
    },
    deduction: {
        type: Number,
        required: true
    },
    registerAt: {
        type: Number,
        default: todayStamp
    }
})

export const typeUserModel = mongoose.model('TypeUser', TypeUser)
export default mongoose.model('User', UsersSchema)
