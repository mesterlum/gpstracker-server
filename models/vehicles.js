import mongoose,{ Schema } from 'mongoose'
import { todayStamp } from '../utils'
const Vehicle = new Schema({
    imei: {
        type: String,
        min: 15,
        required: true,
        unique: true
    },
    matricule: {
        type: String,
        required: true
    },
    rout : {
        type : Schema.Types.ObjectId,
        ref: 'Rout',
        required: true
    },
    employeeRegister: {
        type: Schema.Types.ObjectId,
        ref: 'Employee',
        required: true
    },
    employeeOperator: {
        type : Schema.Types.ObjectId,
        required: true,
        ref: 'Employee'
    },
    service: {
        type : Schema.Types.ObjectId,
        ref: 'ServicesVehicle'
    },
    status: {
        type : String,
        enum: ['A','B'],
        default: 'A',
        required: true
    },
    dateRegister: {
        type: Number,
        default: todayStamp
    }
})

const ServicesVehicle = new Schema({
    price: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: false
    },
    dateRegister: {
        type: Number,
        default: todayStamp
    }
})


export const servicesVehicleModel = mongoose.model('ServicesVehicle', ServicesVehicle)
export const vehicleModel = mongoose.model('Vehicle', Vehicle)