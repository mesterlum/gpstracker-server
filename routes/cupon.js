import { Router } from 'express'
import isAuth from '../isAuth'
import controllerCupons from '../controllers/cupon'


const router = Router()

router.post('/register-cupon', controllerCupons.registerCupon)
router.put('/redeem-cupon', isAuth, controllerCupons.redeemCoupon)

export default router