import { Router } from 'express'

import isAuth from '../isAuth'
import controllerEmployee from '../controllers/employees'


const router = Router()

router.post('/login', controllerEmployee.logIn)
router.post('/signup', isAuth, controllerEmployee.signUp)

export default router