import { Router } from 'express'

//Routes
import Users from './users'
import Employees from './employees'
import Routs from './routs'
import Tracking from './tracking'
import Vehicles from './vehicles'
import Cupons from './cupon'
import Travel from './travel'

const router = Router()

//Users
router.use('/users', Users)

//Employees
router.use('/employees', Employees)

//Routs
router.use('/routes', Routs)

//Tracking
router.use('/tracking', Tracking)

//Vehicles
router.use('/vehicles', Vehicles)

// Cupons
router.use('/cupons', Cupons)

//Travel
router.use('/travels', Travel)


export default router