import { Router } from 'express'
import isAuth from '../isAuth'
import controllerRouts from '../controllers/routs'


const router = Router()

router.post('/registerrout', isAuth, controllerRouts.registerRout)
router.post('/register-point', isAuth, controllerRouts.registerPoint)

router.get('/get-points', controllerRouts.getPoints)
router.get('/get-rout-by-point/:point', controllerRouts.getRoutByPoint)
router.get('/getrout',isAuth, controllerRouts.getRouts)



export default router