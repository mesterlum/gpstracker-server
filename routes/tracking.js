import { Router } from 'express'

import controllerTracking from '../controllers/tracking'


const router = Router()

router.post('/tracking', controllerTracking.registerTracking)
router.post('/trackcoors')

export default router
