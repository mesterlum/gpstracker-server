import { Router } from 'express'

import isAuth from '../isAuth'
import travelController from '../controllers/travel'


const router = Router()

router.post('/register-travel', isAuth, travelController.registerTravel)


export default router