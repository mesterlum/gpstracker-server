import { Router } from 'express'
import controllerUser from '../controllers/users'
import formidable from 'express-formidable'
import isAuth from '../isAuth'

const router = Router()


router.post('/login', controllerUser.logIn)
router.post('/signup', controllerUser.signUp)
router.get('/activation', controllerUser.validateAccount)
router.put('/uploadavatar', [isAuth, formidable()], controllerUser.uploadImageForAvatar)
router.get('/getavatar', isAuth, controllerUser.getAvatar)
router.post('/register-type', isAuth, controllerUser.registerTypeUser)
router.get('/get-information', isAuth, controllerUser.getInformationUser)
export default router