import { Router } from 'express'
import isAuth from '../isAuth'
import controllerVehicle from '../controllers/vehicles'


const router = Router()

router.post('/register-vehicle', isAuth, controllerVehicle.registerVehicle)
router.post('/register-service', isAuth, controllerVehicle.registerService)

router.get('/get-vehicle/:id', isAuth, controllerVehicle.getVehicle)



export default router