import jwt from 'jwt-simple'
import moment from 'moment'
import nodeMailer from 'nodemailer'
import config from './config'
import templateHtml from './assets/html/emailTemplate'

const { SECRET_KEY_TOKEN, GOOGLE_ACCOUNT, GOOGLE_PASSWORD } = config


export function encodeToken(user) {

    let payload = {
        sub: user,
        iat: moment().unix(),
        exp: moment().add(14, "days").unix()
    }

    return jwt.encode(payload, SECRET_KEY_TOKEN)

}

export function decodeToken(token) {

    return new Promise((resolve, reject) => {

        try{

            let payload = jwt.decode(token, SECRET_KEY_TOKEN)

            if(payload.exp <= moment().unix()) {
                reject({status: 401, message: 'Session caducada'})
            }
            resolve(payload)

        }catch(err){
            reject({status: 500, message: 'Internal error'})
        }

    })

}

export function emailConfiguration(to,name, code, idUser){
    
    const mailOptions = {
        from: GOOGLE_ACCOUNT, 
        to,
        subject: 'Completa tu registro GPSTracker',
        html: templateHtml(name,code,idUser)
    }

    return new Promise((resolv, reject) => {

        nodeMailer.createTransport({
            service: 'gmail',
            auth: {
                   user: GOOGLE_ACCOUNT,
                   pass: GOOGLE_PASSWORD
               }
           }).sendMail(mailOptions,(err,info) => {
               if (err) reject(true)
               resolv(true)
           })
    })
       
}
