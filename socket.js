import { vehicleModel } from './models/vehicles'

export default socket => {

    socket.on('connection', socket => {
        socket.on('tracking', data => getTracking(socket, data))
    })
}

const getTracking = async (socket, data) => {
    
    let vehicle = await vehicleModel.findOne({ imei: data.imei })   
        .populate('rout')
        .populate('service')
    if (vehicle)
        socket.broadcast.emit('tracking', {
            gps: data,
            vehicle
        })
}